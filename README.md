# GRRS - Rust Command-Line Tool

GRRS (Grep Replacement) is a simple command-line tool written in Rust for searching patterns in text files. This tool provides functionality similar to the grep command found in Unix-like operating systems.


GRRS is designed to be a lightweight and efficient tool for searching patterns in text files. It's built using Rust, leveraging its performance and safety features. The tool is suitable for various use cases such as log analysis, data processing, and text manipulation.

## Requirements

To build and use GRRS, you need to have the following installed:

* Rust programming language (https://www.rust-lang.org/tools/install)

## Project Structure

The project is organized as follows:

* src/main.rs: Main entry point of the command-line tool.
* src/lib.rs: Contains the core functionality of the tool.
* tests/: Directory containing unit tests for the tool.
* Cargo.toml: Manifest file for Rust dependencies and configuration.

## Usage

To use GRRS, follow these steps:

* Clone the repository to your local machine.
* Navigate to the root directory of the project.
* Build the tool using Cargo:
```
cargo build --release
```

* Once built, you can run the tool with the following command:
```
./target/release/grrs <PATTERN> <FILE>
```

Replace <PATTERN> with the pattern you want to search for and <FILE> with the path to the file you want to search in.


## Testing

GRRS includes comprehensive unit tests to ensure its functionality. To run the tests, execute the following command:

```
cargo test
```
This command will run all the unit tests defined in the tests/ directory and ensure the tool behaves as expected under different scenarios.

## Screenshots
* Screenshot of sample output and tests 
![](sc.png)

* Error handling
![](error.png)